var char = prompt('Введите число');
if (parseInt(char) > 2000000000) {
  console.error('Ошибка: число слишком большое');
} else {
  var char_arr = new Array();
  for (let i = 0; i < char.length; i++) {
    char_arr.push(char[i])
  }

  char_arr.sort((a, b) => {
    return a < b ? -1 : 1
  })

  var result = new String();
  for (let i = 0; i < char_arr.length; i++) {
    result += char_arr[i];
  }

  console.log(result);
}
