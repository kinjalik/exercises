let char = parseInt(prompt('Введите число'));
let greaterSimple = 0;
let lowerSimple = 0;

// Greater Simple
for (let i = char + 1, found = false; found == false; i++) {
  if (isSimple(i)) {
    greaterSimple = i;
    found = true;
  }
}

// Lower Simple
for (let i = char - 1, found = false; found == false; i--) {
  if (isSimple(i)) {
    lowerSimple = i;
    found = true;
  }
}

if (char - lowerSimple > greaterSimple - char) {
  console.log(greaterSimple);
} else {
  console.log(lowerSimple);
}

function isSimple(char) {
  let isSimple = true;
  let divider = 2;

  while (isSimple == true && divider !== char) {
    if (char % divider == 0) return false;
    divider += 1;
  }

  return true;
}