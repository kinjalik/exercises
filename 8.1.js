let char = new Number();
do {
  char = parseInt(prompt('Введите число'));
} while (char < 10);

for (let i = char - 1, counter = 0; counter < 3; i--) {
  if (isSimple(i)) {
    console.log(i);
    counter += 1;
  }
}

function isSimple(char) {
  let isSimple = true;
  let divider = 2;

  while (isSimple == true && divider !== char) {
    if (char % divider == 0) return false;
    divider += 1;
  }

  return true;
}