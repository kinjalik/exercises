let char = parseInt(prompt('Введите число'));
for (let i = char + 1, found = false; found == false; i++) {
  if (isSimple(i)) {
    console.log(i);
    found = true;
  }
}

function isSimple(char) {
  let isSimple = true;
  let divider = 2;

  while (isSimple == true && divider !== char) {
    if (char % divider == 0) return false;
    divider += 1;
  }

  return true;
}