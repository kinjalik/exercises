const char = parseInt(prompt('Введите число'), 10);
let dividersSum = 0;

for (let i = 1; i <= char; i++) {
  if (char % i === 0 && char !== i) dividersSum += i; 
}

console.log('Сумма делителей:', dividersSum);
if (char === dividersSum) console.log('Число является совершенным');
else console.log('Число не является совершенным');