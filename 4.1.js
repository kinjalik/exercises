let start = parseInt(prompt('Введите N'), 10);
const end = parseInt(prompt('Введите M'), 10);
for (; start <= end; start ++) {
  if (isSimple(start)) {
    console.log(start);
  }
}

// Определение простоты

function isSimple(char) {
  let isSimple = true;
  let divider = 2;

  while (isSimple == true && divider !== char) {
    if (char % divider == 0) return false;
    divider += 1;
  }

  return true;
}