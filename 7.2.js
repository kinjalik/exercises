for (let i = 1000; i < 10000; i++) {
  let c1 = i % 10;
  let c2 = parseInt(i % 100 / 10);
  let c3 = parseInt(i % 1000 / 100);
  let c4 = parseInt(i / 1000);

  if (c1 === c4 && c2 === c3) {
    console.log(i);
  }
}