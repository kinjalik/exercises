let chars = new Array();

let inputDone = false;
do {
  let char = prompt('Введите число, нажмите "Отмена" для окончания ввода');
  if (char != null && char != NaN) {
    chars.push(parseInt(char));
  } else {
    inputDone = true;
  }
  console.log(char);
  console.log(inputDone);
} while (inputDone == false);

for (i of chars) {
  let c1 = i % 10;
  let c2 = parseInt(i % 100 / 10);
  let c3 = parseInt(i % 1000 / 100);
  let c4 = parseInt(i / 1000);
  if (c1 != c2 && c1 != c3 && c1 != c4 && c2 != c3 && c2 != c4 && c3 != c4) {
    console.log(i);
  }
}