var resultFound = false;
let i = 100;
while (resultFound == false && i < 1000) {
  if ((i % 100) * 7 === i) {
    resultFound = true;
  } 
  i += 1;
}
console.log(i);