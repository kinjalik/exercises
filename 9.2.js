var chars = {
  _1st: parseInt(prompt('Введите число')),
  _2nd: parseInt(prompt('Введите число'))
}

var dividers = {
  _1st: [],
  _2nd: []
}

var areSimple = true;

for (let i = 2; i <= chars._1st; i++) {
  if (chars._1st % i == 0) {
    dividers._1st.push(i);
    chars._1st /= i;
  };
}

for (let i = 2; i <= chars._2nd; i++) {
  if (chars._2nd % i == 0) {
    dividers._2nd.push(i);
    chars._2nd /= i;
  };
}

console.log(`Делители первого числа:`);
console.log(dividers._1st);
console.log(`Делители второго числа:`);
console.log(dividers._2nd);

for (char_1st of dividers._1st) {
  for (char_2nd of dividers._2nd) {
    console.log(`Пара: ${char_1st} - ${char_2nd}`);
    if (char_1st === char_2nd) areSimple = false;
  }
}

console.log(areSimple ? 'Числа взаимно простые' : 'Числа не взаимно простые');