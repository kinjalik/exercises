let char = parseInt(prompt('Введите число'));

if (isSimple(char - 1) && isSimple(char + 1)) {
  console.log('Соседние числа - близнецы');
} else {
  console.log('Сосдение числа - НЕ близнецы');
}

function isSimple(char) {
  let isSimple = true;
  let divider = 2;

  while (isSimple == true && divider !== char) {
    if (char % divider == 0) return false;
    divider += 1;
  }

  return true;
}