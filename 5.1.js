let char = parseInt(prompt('Введите число'));
for (let counter = 0; counter < 5; char++) {
  if (isSimple(char)) {
    counter += 1;
    console.log(char)
  }
}

function isSimple(char) {
  let isSimple = true;
  let divider = 2;

  while (isSimple == true && divider !== char) {
    if (char % divider == 0) return false;
    divider += 1;
  }

  return true;
}
