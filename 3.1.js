const char = parseInt(prompt('Введите число'), 10);
let isSimple = true;
let divider = 2;

while (isSimple == true && divider !== char) {
  if (char % divider == 0) isSimple = false;
  divider += 1;
}

console.log(`${isSimple == true ? 'Число простое' : 'Число сложное'}`);
