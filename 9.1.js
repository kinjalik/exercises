let min = parseInt(prompt('Введите начало диапозона'));
let max = parseInt(prompt('Введите конец диапозона'));

let firstSimple, secondSimple;

for (; min < max; min++) {
  firstSimple = secondSimple;
  for (let i = min + 1, found = false; found == false; i++) {
    if (isSimple(i)) {
      secondSimple = i;
      found = true;
    }
  }

  if (secondSimple - firstSimple === 2) console.log(firstSimple, secondSimple);

}

function isSimple(char) {
  let isSimple = true;
  let divider = 2;

  while (isSimple == true && divider !== char) {
    if (char % divider == 0) return false;
    divider += 1;
  }

  return true;
}